# some of the code was taken
# from my previous project,
# the AniParser Bot, made by me :)

# github.com/prinnevald/AniParser_Redux/

from bs4 import BeautifulSoup
import urllib.request
import requests
import time

from apscheduler.schedulers.blocking import BlockingScheduler


# news
tengri = 'https://tengrinews.kz'
inform = 'https://www.inform.kz'
kapital = 'https://kapital.kz/news'

# agent
user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
headers = {'User-Agent': user_agent,} 

# news post
class aPost():
    def __init__(self, name, src):
            self.title = name
            self.link = src

# get soup
def get_soup(web):
    req = urllib.request.Request(web, None, headers)
    resp = urllib.request.urlopen(req)
    Data = resp.read()
    return BeautifulSoup(Data, 'html.parser')


# get list of news title and link from tengri
def get_tengri():
    soup = get_soup(tengri)
    news = soup.find_all('div', {"class": "tn-main-news-grid"})[0].find_all('div', {"class": "tn-main-news-item"})
    for post in news:
        title = post.find('span', {"class": "tn-main-news-title"}).text
        src = tengri + post.find('a', {"class": "tn-link"}).get('href')
        yield aPost(title, src)


# get list of news title and link from inform
def get_inform():
    soup = get_soup(inform)
    news = soup.find_all('article', {"class": "anounce-news"})
    for post in news:
        title = post.find('h3', {"class": "anounce-news__name"}).text
        src = inform + post.find('a', {"class": "anounce-news__link"}).get('href')
        yield aPost(title, src)


# get list of news title and link from kapital
def get_kapital():
    soup = get_soup(kapital)
    news = soup.find_all('article', {"class": "main-news__item"})
    for post in news:
        tmp = post.find('a', {"class": "main-news__name"})
        yield aPost(tmp.text, 'https://kapital.kz' + tmp.get('href'))


def main():
    inform = get_inform()

    for i in inform:
        print(i.title)
        requests.post("http://20.163.220.138:80/put", json={"link":i.link, "title":i.title})
        time.sleep(1)

    tengri = get_tengri()

    for t in tengri:
        print(t.title)
        requests.post("http://20.163.220.138:80/put", json={"link":t.link, "title":t.title})
        time.sleep(1)

    kapital = get_kapital()

    for k in kapital:
        print(k.title)
        requests.post("http://20.163.220.138:80/put", json={"link":k.link, "title":k.title})
        time.sleep(1)

scheduler = BlockingScheduler()
scheduler.add_job(main, 'interval', hours=1)
scheduler.start()
