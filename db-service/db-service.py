from fastapi import FastAPI, UploadFile, File, Form, Request
from fastapi.middleware.cors import CORSMiddleware
import sqlite3
import uvicorn

from enum import Enum

sqliteConnection = sqlite3.connect('newsDB.db')
cursor = sqliteConnection.cursor()

app = FastAPI()

@app.on_event("shutdown")
async def database_disconnect():
    sqliteConnection.close()

@app.get("/search")
async def fetch_data(info: Request):
    info = await info.json()
    query = "SELECT * FROM news WHERE title LIKE '%"+info['query']+"%'"
    results = cursor.execute(query)
    return results

@app.post("/put")
async def put_data(info: Request):
    info = await info.json()
    query = "INSERT OR IGNORE INTO news(link, title) VALUES (?, ?)"
    cursor.execute(query, (info["link"], info["title"]))
    sqliteConnection.commit()
    return "ok"

@app.get("/")
async def root():
    return {"message": "Hello World"}

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=80, log_level="info")
