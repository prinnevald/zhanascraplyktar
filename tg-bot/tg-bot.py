# some of the code was taken
# from my previous project,
# the AniParser Bot, made by me :)

# github.com/prinnevald/AniParser_Redux/

# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import urllib.request
import requests
import telebot
import json

tk = '6065717891:AAFacG_EYU_PPhMQZ6U0tovKUoNgx9p_Mqk'
bot = telebot.TeleBot(tk)


# news
tengri = 'https://tengrinews.kz'
inform = 'https://www.inform.kz'
kapital = 'https://kapital.kz/news'

# agent
user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
headers = {'User-Agent': user_agent,} 

# news post
class aPost():
    def __init__(self, name, src):
            self.title = name
            self.link = src

# get soup
def get_soup(web):
    req = urllib.request.Request(web, None, headers)
    resp = urllib.request.urlopen(req)
    Data = resp.read()
    return BeautifulSoup(Data, 'html.parser')


# get list of news title and link from tengri
def get_tengri():
    soup = get_soup(tengri)
    news = soup.find_all('div', {"class": "tn-main-news-grid"})[0].find_all('div', {"class": "tn-main-news-item"})
    for post in news:
        title = post.find('span', {"class": "tn-main-news-title"}).text
        src = tengri + post.find('a', {"class": "tn-link"}).get('href')
        yield aPost(title, src)


# get list of news title and link from inform
def get_inform():
    soup = get_soup(inform)
    news = soup.find_all('article', {"class": "anounce-news"})
    for post in news:
        title = post.find('h3', {"class": "anounce-news__name"}).text
        src = inform + post.find('a', {"class": "anounce-news__link"}).get('href')
        yield aPost(title, src)


# get list of news title and link from kapital
def get_kapital():
    soup = get_soup(kapital)
    news = soup.find_all('article', {"class": "main-news__item"})
    for post in news:
        tmp = post.find('a', {"class": "main-news__name"})
        yield aPost(tmp.text, 'https://kapital.kz' + tmp.get('href'))


######### HOWTO #################
# all of the get_name functions return a list of objects
# item.title -> title and item.link -> link to the news


@bot.message_handler(commands=['start', 'help'])
def startBot(message):
    bot.send_message(message.from_user.id, "Welcome to Zhanascraplyktar!")
    bot.send_message(message.from_user.id, "Type /tengri to see latest Tengri News.")
    bot.send_message(message.from_user.id, "Type /inform to see latest Inform News.")
    bot.send_message(message.from_user.id, "Type /kapital to see latest Kapital News.")
    bot.send_message(message.from_user.id, "Type /search YOURWORD to search within recent news among all three!")


@bot.message_handler(commands=['tengri'])
def latest_tengri(message):
    bot.send_message(message.from_user.id, "Latest Tengri News are:")
    
    posts = get_tengri()
    for p in posts:
        bot.send_message(message.from_user.id, p.title + '\n' + p.link)

    bot.send_message(message.from_user.id, "These were all the latest news!")

@bot.message_handler(commands=['inform'])
def latest_inform(message):
    bot.send_message(message.from_user.id, "Latest Inform News are:")
    
    posts = get_inform()
    for p in posts:
        bot.send_message(message.from_user.id, p.title + '\n' + p.link)

    bot.send_message(message.from_user.id, "These were all the latest news!")

@bot.message_handler(commands=['kapital'])
def latest_kapital(message):
    bot.send_message(message.from_user.id, "Latest Kapital News are:")
    
    posts = get_kapital()
    for p in posts:
        bot.send_message(message.from_user.id, p.title + '\n' + p.link)

    bot.send_message(message.from_user.id, "These were all the latest news!")


@bot.message_handler(commands=['search'])
def search(message):
    bot.send_message(message.from_user.id, "Search results for " + message.text + ":")
    s = message.text.replace('/search ', '')
    print(s)

    res = requests.get("http://20.163.220.138:80/search", json={"query":s}).json()

    for p in res:
        print(type(p))
        bot.send_message(message.from_user.id, p)

    bot.send_message(message.from_user.id, "These are the results")

bot.infinity_polling()
